package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithShield implements DefenseBehavior {

    public String defend() {
        return "fire in the hole";
    }
    public String getType() {
        return "Shield";
    }
}
