[33mcommit a7afd826636f1db1514997b6ffab97acc2254f33[m[33m ([m[1;36mHEAD -> [m[1;32mmaster[m[33m, [m[1;31morigin/master[m[33m)[m
Merge: 53f2c12 5362fed
Author: billyvande7 <billyvande7@gmail.com>
Date:   Thu Feb 25 13:27:00 2021 +0700

    Merge branch 'tutorial-0' , split to 'rumah house'

[33mcommit 53f2c120d9cec7ee641b30894825cc340c61b90f[m
Author: billyvande7 <billyvande7@gmail.com>
Date:   Thu Feb 25 13:24:44 2021 +0700

    change home to rumah

[33mcommit 5362fed72a9a78c02c020b75526d985a55971a9a[m[33m ([m[1;31morigin/tutorial-0[m[33m, [m[1;32mtutorial-0[m[33m)[m
Author: billyvande7 <billyvande7@gmail.com>
Date:   Thu Feb 25 13:22:44 2021 +0700

    change home to house

[33mcommit 5d9d7fa6fa577cd69da01de683d84ed841e300ac[m[33m ([m[1;31mupstream/master[m[33m, [m[1;31mupstream/HEAD[m[33m)[m
Merge: a576eb2 72cc4b8
Author: Adrika Novrialdi <adrika.novrialdi@gmail.com>
Date:   Tue Feb 23 09:02:18 2021 +0000

    Merge branch 'dev-tutorial-0' into 'master'
    
    Release tutorial 0 version 1.1
    
    See merge request csui-advprog-2019/advprog-tutorial-2021!1

[33mcommit 72cc4b87a54faa194dbcddec95eb78681275c570[m
Author: adrika-novrialdi <adrika.novrialdi@gmail.com>
Date:   Tue Feb 23 16:00:47 2021 +0700

    Fix typo

[33mcommit a576eb2d14a898abf53e6695285f5d30caf86866[m
Merge: 0af6d0f c148707
Author: Adrika Novrialdi <adrika.novrialdi@gmail.com>
Date:   Tue Feb 23 08:46:11 2021 +0000

    Merge branch 'dev-tutorial-0' into 'master'
    
    Tutorial 0 Release
    
    See merge request adrika-novrialdi/advrprog-tutorial-2021-dev!1

[33mcommit c14870773af82d21bfce338c73b00ab49e8796bc[m
Author: adrika-novrialdi <adrika.novrialdi@gmail.com>
Date:   Tue Feb 23 15:38:20 2021 +0700

    Delete 2020 tutorial residue

[33mcommit 0af6d0f5597b836da0f8bfb24b43894f4b3e374b[m
Author: adrika-novrialdi <adrika.novrialdi@gmail.com>
Date:   Tue Feb 23 15:29:40 2021 +0700

    Edit readme.md tutorial 0, delete gitlab CI

[33mcommit efd5d071cd2f233fa8747f9b199c11c58aa664d4[m
Author: adrika-novrialdi <adrika.novrialdi@gmail.com>
Date:   Tue Feb 23 15:10:41 2021 +0700

    Initialize Advprog Tutorial
